# Free Flutter intermediate course 1 hour learn new widgets # 

## First App ##
This app demos how to use GridView

![Screenshot](images/01_gridview_flutter_app.png)

## Second App ##
This app demos how to use Expanded and Flex elements

![Screenshot](images/02_expanded_and_flex_flutter.png)

## Third App ##
This app demos How to use IOS widgets

![Screenshot](images/03_android_view.png)
![Screenshot](images/03_ios_view.png)

## Fourth App ##
This app parse www json data title and body

![Screenshot](images/04_json.png)

## Fift App ##
This app demos different Buttons functionalities (snackbar, modal and simpledialog).
First mainpage, second snackbar pressed, third modal button pressed and fourth simpledialog button pressed.

![Screenshot](images/05_01main.png) ![Screenshot](images/05_02snackbar.png)

![Screenshot](images/05_03modalbutton.png) ![Screenshot](images/05_04simpledialog.png)

## Sixth App ##
This app demos PageView widgets.

![Screenshot](images/06_01.png) ![Screenshot](images/06_02.png) ![Screenshot](images/06_03.png)

## Seventh App ##

This app demos save data with shared preferences

![Screenshot](images/07_01.png) ![Screenshot](images/07_02.png)

## Eight App ##

This app demos how to use sliverList. First you need SliverAppBar and then inside SliverList object you have to create Card object:

![Screenshot](images/08_01.png) ![Screenshot](images/08_02.png)

## Ninth App ##

This app demos Stack widgets. That means two widgets is on top of eack other.

![Screenshot](images/09_01.png)