import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

//import 'package:flutter_json/post.dart';
import 'package:path_provider/path_provider.dart'; //add path provider dart plugin on pubs
import 'post.dart';
import 'dart:convert';
import 'dart:io';


void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<List<Post>> showPosts() async {
    var data = await http.get("https://jsonplaceholder.typicode.com/posts");
    var dataDecoded = json.decode(data.body);

    List<Post> posts = List();
    dataDecoded.forEach((post) {
      String title = post["title"];
      if (title.length > 25.0) {
        title = post["title"].substring(1, 25) + "...";
      }
      String body = post["body"].replaceAll(RegExp(r'\n'), " ");
      posts.add(Post(title, body));
    });
    return posts;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Json'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: FutureBuilder(
            future: showPosts(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(snapshot.data[index].title,
                                  style: TextStyle(fontSize: 30.0)),
                              Divider(),
                              Text(snapshot.data[index].text,
                                  style: TextStyle(fontSize: 15.0)),
                              Divider(),
                              RaisedButton(
                                  child: Text("Read more ..."),
                                  onPressed: () {},


                              )
                            ],
                          ),
                        ),
                      );
                    });

              }else{
                return Align(
                  alignment: FractionalOffset.center,
                  child: CircularProgressIndicator(),
                );
              }
            }
        ),
      ),
    );
  }
}
