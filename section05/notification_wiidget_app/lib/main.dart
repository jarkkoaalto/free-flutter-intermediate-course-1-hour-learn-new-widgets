import 'package:flutter/material.dart';

void main() =>
    runApp(MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Notifications"),
        ),
        body: MyApp(),
      ),
    ));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  void showSnackbar(){
    final snackbar = SnackBar(
      content: Text("Hi Snackbar here"),
      action: SnackBarAction(
          label: "Ok",
          onPressed: (){

          }),
    );
  Scaffold.of(context).showSnackBar(snackbar);
  }

  void showButton(){
      showModalBottomSheet(
          context: context,
          builder: (builder){
            return Container(
              height: 50.0,
              color: Colors.deepOrange,
              child: Center(
                child: Text("Bottom sheet Modal"),
              ),
            );
          }
          );
  }

  void showSimpleDialog() async{
    return showDialog(
        context: context,
      builder: (BuildContext context){
          return AlertDialog(
            title: Text("Simple Dialog"),
            content: Text("The message you want"),
            actions: <Widget>[
              FlatButton(
                child: Text("Done"),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              )
            ],
          );
      }
    );

  }



  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: MaterialButton(
            child: Text("Snack Bar"),
            color: Colors.lightBlue,
            textColor: Colors.white,
            splashColor: Colors.green,
            onPressed: () {
              showSnackbar();
            },
          ),
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: MaterialButton(
            child: Text("Modal Button"),
            color: Colors.lightBlue,
            textColor: Colors.white,
            splashColor: Colors.green,
            onPressed: () {
              showButton();

            },
          ),
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: MaterialButton(
            child: Text("Simple Dialog"),
            color: Colors.lightBlue,
            textColor: Colors.white,
            splashColor: Colors.green,
            onPressed: () {
              showSimpleDialog();

            },
          ),
        ),
      ],
    );
  }
}
