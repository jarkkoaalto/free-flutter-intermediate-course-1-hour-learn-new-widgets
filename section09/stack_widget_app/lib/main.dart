import 'package:flutter/material.dart';

void main() => runApp(
    MaterialApp(
      home: MyApp()
    )
);

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

 /*
 topLeft, topCenter, topRight, center
  */

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          Container(
            width: 300,
            height: 300,
            color: Colors.green,
          ),
          Image(
            width: 200,
            height: 200,
            image: AssetImage("images/index.jpeg"),
          )
        ],
      ),
    );
  }
}

